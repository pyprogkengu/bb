from django.contrib import admin

from .models import Bb, Rubric


# Register your models here.

@admin.register(Bb)
class BbAdmin(admin.ModelAdmin):
    list_display = ('title', 'content', 'price', 'published', 'rubric')
    list_display_links = ('title', 'content')
    search_fields = ('title', 'content')


@admin.register(Rubric)
class Rubric(admin.ModelAdmin):
    pass
